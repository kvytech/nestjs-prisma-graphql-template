import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ROLES_KEY } from '../decorators/authorize_role.decorator';
import { RoleType } from '@prisma/client'
import { GqlExecutionContext } from '@nestjs/graphql';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private userService: UsersService
  ) {}

  async canActivate(context: ExecutionContext) {
    const requiredRoles = this.reflector.getAllAndOverride<RoleType[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!requiredRoles) {
      return true;
    }
    const request = GqlExecutionContext.create(context).getContext().req
    const workspaceId = request.headers['workspaceid']
    if (!workspaceId) {
      return true;
    }

    const role = await this.userService.roleInWorkspace(request.user, workspaceId)
    return requiredRoles.includes(role)
  }
}
