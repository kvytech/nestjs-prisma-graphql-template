import { applyDecorators } from '@nestjs/common';
import { SetMetadata, UseGuards } from '@nestjs/common';
import { RoleType } from '@prisma/client';
import { RolesGuard } from '../guards/role.guard'

export const ROLES_KEY = 'roles';

export function AuthorizeRoles(...roles: RoleType[]) {
  return applyDecorators(
    SetMetadata(ROLES_KEY, roles),
    UseGuards(RolesGuard)
  );
}
