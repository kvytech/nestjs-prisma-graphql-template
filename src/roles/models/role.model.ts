import 'reflect-metadata';
import {
  ObjectType,
  registerEnumType,
  Field,
} from '@nestjs/graphql';
import { BaseModel } from 'src/common/models/base.model';
import { User, Workspace, RoleType } from '@prisma/client';
import { User as UserModel } from 'src/users/models/user.model'
import { Workspace as WorkspaceModel } from 'src/workspaces/models/workspace.model'

registerEnumType(RoleType, {
  name: 'RoleType',
  description: 'User role',
});

@ObjectType()
export class Role extends BaseModel {
  @Field(() => RoleType)
  kind: RoleType;

  @Field(() => UserModel)
  user: User;

  @Field(() => WorkspaceModel)
  workspace: Workspace
}
