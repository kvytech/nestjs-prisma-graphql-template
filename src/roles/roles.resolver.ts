import { Resolver, ResolveField, Parent } from '@nestjs/graphql';
import { PrismaService } from 'nestjs-prisma';
import { Role } from 'src/roles/models/role.model';

@Resolver(() => Role)
export class RolesResolver {
  constructor(
    private readonly prisma: PrismaService
  ) {}

  @ResolveField('workspace')
  workspace(@Parent() role: Role) {
    return this.prisma.role.findUnique({ where: { id: role.id } }).workspace();
  }
}
