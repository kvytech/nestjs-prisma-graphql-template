import 'reflect-metadata';
import {
  ObjectType,
  Field,
} from '@nestjs/graphql';
import { BaseModel } from 'src/common/models/base.model';
import { Role } from '@prisma/client';
import { Role as RoleModel } from 'src/roles/models/role.model';

@ObjectType()
export class Workspace extends BaseModel {
  @Field(() => String)
  title: string;

  @Field(() => [RoleModel])
  roles: Role[]
}
